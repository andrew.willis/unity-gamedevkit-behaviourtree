﻿using System;


namespace UMNP.GamedevKit.BehaviourTree
{
    /// <summary>
    /// Executes the node behaviour one after another.<br/>
    /// Returns State.Fail if one of its children failed.
    /// </summary>
    public class Sequence : Node
    {
        private Node[] _nodes = new Node[] { };

        public Sequence(Node[] nodes) =>
            _nodes = nodes;

        public override void Clear()
        {
            foreach (var node in _nodes)
                node.Clear();

            Array.Clear(_nodes, 0, _nodes.Length);
            _nodes = null;
        }

        public override State Evaluate()
        {
            var isAnyNodeRunning = false;
            foreach (var node in _nodes)
            {
                switch (node.Evaluate())
                {
                    case State.Success:
                        break;
                    case State.Fail:
                        return State.Fail;
                    case State.Running:
                        isAnyNodeRunning = true;
                        break;
                    default:
                        break;
                }
            }

            return isAnyNodeRunning ? State.Running : State.Success;
        }
    }
}