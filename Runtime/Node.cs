﻿/*
Code is taken from GameDevChef on YouTube
See link:
https://github.com/GameDevChef/BehaviourTrees
*/

using System;

namespace UMNP.GamedevKit.BehaviourTree
{
    [Serializable]
    public abstract class Node
    {
        public abstract State Evaluate();

        public abstract void Clear();

        public enum State
        {
            Success,
            Fail,
            Running
        }
    }
}
