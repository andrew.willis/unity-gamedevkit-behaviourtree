﻿using System;

namespace UMNP.GamedevKit.BehaviourTree
{
    /// <summary>
    /// Walks through the child nodes to find one that succeeds first (order matters).<br/>
    /// Returns State.Fail if none succeeded.
    /// </summary>
    public class Fallback : Node
    {
        private Node[] _nodes = new Node[] { };

        public Fallback(Node[] nodes) =>
            _nodes = nodes;

        public override void Clear()
        {
            foreach (var node in _nodes)
                node.Clear();

            Array.Clear(_nodes, 0, _nodes.Length);
            _nodes = null;
        }

        public override State Evaluate()
        {
            foreach (var node in _nodes)
            {
                switch (node.Evaluate())
                {
                    case State.Success:
                        return State.Success;
                    case State.Fail:
                        break;
                    case State.Running:
                        return State.Running;
                    default:
                        break;
                }
            }

            return State.Fail;
        }
    }
}
